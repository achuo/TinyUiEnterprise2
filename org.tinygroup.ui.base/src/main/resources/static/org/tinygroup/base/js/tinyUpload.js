$.fn.tinyUpload=function(options) {
    if(!this.val()||!window.webInfo||!window.webInfo.isSafeBrowser) return false;
    var files=this.get(0).files;
    var defaultOpt={
        type:"post",
        dataType:"json",
        data:{}
    };
    var formInputName=$(this).attr("name");
    options=$.extend({},defaultOpt,options);
    if(window.webInfo.uploadType=="oss"){
        var hasBack=false,dir="";
        var fixData={};
        $.each(options.data,function(key,value){
            if(key.indexOf("x:")==-1){
                key="x:"+key.replace(/([A-Z])/g,"_$1").toLowerCase();
            }
            fixData[key]=value;
        });

        $.ajax({
            url: options.url,
            async:false,
            dataType: "json",
            success: function (obj) {
                var new_multipart_params = {
                    //'key': obj['dir'] +moment().format("YYYY-MM-DD") + "/" + fileName,
                    'policy': obj['policy'],
                    'OSSAccessKeyId': obj['accessid'],
                    'success_action_status': '200', //让服务端返回200,不然，默认会返回204
                    'callback': obj['callback'],
                    'signature': obj['signature'],
                    'expire': obj['expire']
                };
                dir=obj['dir'];
                options.url=obj["host"];
                options.data = $.extend(new_multipart_params,fixData);
                hasBack=true;
                formInputName="file";
            }
        });
        if(!hasBack) return false;
    }
    for(var fileIndex=0,len=files.length;fileIndex<len;fileIndex++){
        var fd = new FormData();
        var file=files[fileIndex];
        var fileName=file.name;
        options.data['x:original']=fileName;
        var file_ext=fileName.substr(fileName.lastIndexOf("."));
        var myDate=new Date();
        fileName=moment().format("X")+myDate.getMilliseconds()+file_ext;
        fileName=fileName.toLowerCase();
        options.data['key']=dir +moment().format("YYYY-MM-DD") + "/" + fileName;
        $.each(options.data,function(key,value){
            fd.append(key,value);
        });
        fd.append(formInputName, file);
        $.ajax({
            url:options.url,
            type:options.type,
            processData:false,
            contentType:false,
            data:fd,
            success:function(data){
                if(options.dataType=="json"&&typeof(data)=="string" ){
                    data = data.substring(data.indexOf("{"), data.lastIndexOf("}")+1);
                    data=$.parseJSON(data);
                }
                options.success&&options.success(data);
            },
            beforeSend: function () {
                options.beforeSend&&options.beforeSend();
            },

            error: function (XMLHttpRequest, textStatus, errorThrown) {
                options.error&&options.error(XMLHttpRequest, textStatus, errorThrown);
            },

            complete: function () {
                options.complete&&options.complete();
            }
        })
    }
    return true;
};


jQuery.extend({
    tinyBlobUpload: function (options) {
        if (!options.blob) return false;
        var files = this.get(0).files;
        var defaultOpt = {
            type: "post",
            dataType: "json",
            fileName:"image.png",
            data: {}
        };
        var formInputName = options.name;
        options = $.extend({}, defaultOpt, options);
        if (window.webInfo.uploadType == "oss") {
            var hasBack = false, dir = "";
            var fixData = {};
            $.each(options.data, function (key, value) {
                fixData["x:" + key.replace(/([A-Z])/g, "_$1").toLowerCase()] = value;
            });

            $.ajax({
                url: options.url,
                async: false,
                dataType: "json",
                success: function (obj) {
                    var new_multipart_params = {
                        //'key': obj['dir'] +moment().format("YYYY-MM-DD") + "/" + fileName,
                        'policy': obj['policy'],
                        'OSSAccessKeyId': obj['accessid'],
                        'success_action_status': '200', //让服务端返回200,不然，默认会返回204
                        'callback': obj['callback'],
                        'signature': obj['signature'],
                        'expire': obj['expire']
                    };
                    dir = obj['dir'];
                    options.url = obj["host"];
                    options.data = $.extend(new_multipart_params, fixData);
                    hasBack = true;
                    formInputName = "file";
                }
            });
            if (!hasBack) return false;
        }

        var fd = new FormData();

        var fileName;
        options.data['x:original'] = options.fileName;
        var file_ext = ".png";
        var myDate = new Date();
        fileName = moment().format("X") + myDate.getMilliseconds() + file_ext;
        fileName = fileName.toLowerCase();
        options.data['key'] = dir + moment().format("YYYY-MM-DD") + "/" + fileName;
        $.each(options.data, function (key, value) {
            fd.append(key, value);
        });
        fd.append(formInputName, options.blob, options.fileName);
        $.ajax({
            url: options.url,
            type: options.type,
            processData: false,
            contentType: false,
            data: fd,
            success: function (data) {
                if (options.dataType == "json" && typeof(data) == "string") {
                    data = data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1);
                    data = $.parseJSON(data);
                }
                options.success && options.success(data);
            },
            beforeSend: function () {
                options.beforeSend && options.beforeSend();
            },

            error: function (XMLHttpRequest, textStatus, errorThrown) {
                options.error && options.error(XMLHttpRequest, textStatus, errorThrown);
            },

            complete: function () {
                options.complete && options.complete();
            }
        })

        return true;
    }
})
